﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

////
 // Процедура: ПередЗаписью
 //   Обработчик события ПередЗаписью набора записей регистра.
 //   Проверяет наличие дубликатов перед записью набора в регистр сведений.
 //
 // Параметры:
 //   Отказ {Булево}
 //     При установке в значение Истина, запись не выполняется.
 //   Замещение {Булево}
 //     Признак замещения существующих записей.
 ///
Процедура ПередЗаписью(Отказ, Замещение)
	 Замещение = Истина;
КонецПроцедуры

#КонецЕсли
Процедура ПриЗаписи(Отказ, Замещение)
	
КонецПроцедуры