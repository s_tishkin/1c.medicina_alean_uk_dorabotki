﻿
////
 // Функция: НОДВремениВыполненияУслуг
 //   Служит для определения наибольшего общего делителя времени выполнения услуг на рабочих местах.
 //
 // Параметры:
 //   РабочиеМеста
 //   Дата
 //   Номенклатура
 //     Массив номенклатуры, ограничивающий список спецификаций, по которым определяется НОД.
 //     Если значение не определено, то ограничение по номенклатуре не применяется.
 //
 // Возврат: {Соответствие}
 ///
 &Вместо("НОДВремениВыполненияУслуг")
Функция НОДВремениВыполненияУслуг_Расширение(
	РабочиеМеста, Дата = Неопределено, Номенклатура = Неопределено
) Экспорт
	Запрос_ = Новый Запрос(
		"ВЫБРАТЬ
		|	РАЗНОСТЬДАТ(ДАТАВРЕМЯ(1, 1, 1), ВЫБОР
		|			КОГДА МедицинскиеРабочиеМеста.ВремяВыполнения = ДАТАВРЕМЯ(1, 1, 1)
		|				ТОГДА СпецификацияЭтапыВыполнения.ВремяВыполнения
		|			ИНАЧЕ МедицинскиеРабочиеМеста.ВремяВыполнения
		|		КОНЕЦ, СЕКУНДА) КАК ВремяВыполнения,
		|	СпецификацияМестаВыполненияЭтапов.МестоВыполнения КАК РабочееМесто
		|ИЗ
		|	РегистрСведений.ОсновныеСпецификацииМедицинскихУслуг.СрезПоследних(
		|			&Дата,
		|			&ВсяНоменклатура
		|				ИЛИ Номенклатура В (&Номенклатура)) КАК ОсновныеСпецификации
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.СпецификацииМедицинскихУслуг КАК Спецификации
		|			ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.СпецификацииМедицинскихУслуг.МестаВыполненияЭтапов КАК СпецификацияМестаВыполненияЭтапов
		|				ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.СпецификацииМедицинскихУслуг.ЭтапыВыполнения КАК СпецификацияЭтапыВыполнения
		|				ПО СпецификацияМестаВыполненияЭтапов.Ссылка = СпецификацияЭтапыВыполнения.Ссылка
		|					И СпецификацияМестаВыполненияЭтапов.КлючСтрокиЭтапа = СпецификацияЭтапыВыполнения.КлючСтроки
		|				ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.МедицинскиеРабочиеМеста КАК МедицинскиеРабочиеМеста
		|				ПО СпецификацияМестаВыполненияЭтапов.МестоВыполнения = МедицинскиеРабочиеМеста.Ссылка
		|			ПО (СпецификацияМестаВыполненияЭтапов.Ссылка = Спецификации.Ссылка)
		|		ПО ОсновныеСпецификации.СпецификацияМедицинскойУслуги = Спецификации.Ссылка
		|ГДЕ
		|	МедицинскиеРабочиеМеста.Ссылка В(&РабочиеМеста)
		|	И Спецификации.ПометкаУдаления = ЛОЖЬ"
	);
	Запрос_.УстановитьПараметр("Дата", ?(Дата = Неопределено, ТекущаяДатаСеанса(), Дата));
	Запрос_.УстановитьПараметр("ВсяНоменклатура", Номенклатура = Неопределено);
	Запрос_.УстановитьПараметр("Номенклатура", ?(Номенклатура = Неопределено, Новый Массив, Номенклатура));
	Запрос_.УстановитьПараметр("РабочиеМеста", РабочиеМеста);
	Результат_ = Запрос_.Выполнить();
	Выборка_ = Результат_.Выбрать();
	НОДы_ = Новый Соответствие();
	Для Каждого РабочееМесто_ Из РабочиеМеста Цикл
		НОДы_[РабочееМесто_] = 0;		
	КонецЦикла;
	Пока Выборка_.Следующий() Цикл
		Если Выборка_.ВремяВыполнения <> 0 Тогда
			НОД_ = АлгоритмыДляКоллекций.НОД(Выборка_.ВремяВыполнения, НОДы_[Выборка_.РабочееМесто]);
			НОДы_[Выборка_.РабочееМесто] = НОД_;
		КонецЕсли;
	КонецЦикла;
	Возврат НОДы_;	
КонецФункции


// Используется в процедуре ПерепланироватьУслуги модуля ПланированиеУслугКлиент.
// Вызывается после завершения планирования. Проводит документы ОтменаРезервированияРМ
// и РезервированиеРМ.
//
// Параметры:
//   РезервированиеСсылка - - Ссылка на документ резервирования.
//   ОтменаРезервированияСсылка - - Ссылка на документ отмены резервирования
//
// Возвращаемое значение:
//   Массив - перепланированные услуги.
//
&Вместо("ОбработатьРезультатПланирования")
Функция ОбработатьРезультатПланирования_Расширение(РезервированиеСсылка, ОтменаРезервированияСсылка) Экспорт
	Результат_ = Новый Структура("ОК, Услуги, Приемы", Истина, Новый Массив, Новый Массив);
	Если Не ЗначениеЗаполнено(РезервированиеСсылка) Тогда
		Возврат Результат_;
	КонецЕсли;
	РезервированиеОбъект = РезервированиеСсылка.ПолучитьОбъект();
	Попытка
		НачатьТранзакцию(РежимУправленияБлокировкойДанных.Управляемый);
		Если РезервированиеОбъект.МедицинскиеУслуги.Количество() > 0 Или
			 РезервированиеОбъект.Приемы.Количество() > 0
		Тогда
			Если ЗначениеЗаполнено(ОтменаРезервированияСсылка) И ОтменаРезервированияСсылка.Проведен = Ложь Тогда
				ОтменаРезервированияОбъект = ОтменаРезервированияСсылка.ПолучитьОбъект();
				ОтменаРезервированияОбъект.ДополнительныеСвойства.Вставить("ЭтоПерепланирование", Истина);
				ОтменаРезервированияОбъект.Записать(РежимЗаписиДокумента.Проведение);
			КонецЕсли;
			РезервированиеОбъект.Записать(РежимЗаписиДокумента.Проведение);
		КонецЕсли;
		ЗафиксироватьТранзакцию();
	Исключение
		ОтменитьТранзакцию();
		ОбщиеМеханизмы.ОбработатьОшибку(ИнформацияОбОшибке());
		Результат_.ОК = Ложь;
	КонецПопытки;
	
	Если Результат_.ОК = Истина Тогда
		Колонки_ = "УникальныйИдентификаторУслуги, КлючСтрокиЭтапа, Номенклатура," + 
			"МедицинскоеРабочееМесто,ЗапланированноеВремя";
		Услуги_ = РезервированиеОбъект.МедицинскиеУслуги.Выгрузить(, Колонки_);
		Результат_.Услуги = ОбщегоНазначения.ТаблицаЗначенийВМассив(Услуги_);
		
		Колонки_ = "УникальныйИдентификаторПриема, Номенклатура, МедицинскоеРабочееМесто,ЗапланированноеВремя";
		Приемы_ = РезервированиеОбъект.Приемы.Выгрузить(, Колонки_);
		Результат_.Приемы = ОбщегоНазначения.ТаблицаЗначенийВМассив(Приемы_);
	КонецЕсли;
	Возврат Результат_;
КонецФункции


// Определить свободный кабинет.
//
&Вместо("ОпределитьСвободныйКабинет") 
Функция ОпределитьСвободныйКабинет_Расширение(РабочееМесто, Время, ЧислоТалонов, ДоляЗанятости, КонтрольЗанятости) Экспорт
	Результат_ = Новый Структура("ОК, Кабинет");
	
	Кабинет_ = МедицинскиеРабочиеМеста.ПолучитьКабинетТалона(РабочееМесто, Время);
	Если Не ЗначениеЗаполнено(Кабинет_) Тогда
		Кабинет_ = МедицинскиеРабочиеМеста.ПолучитьКабинет(РабочееМесто, Время);
	КонецЕсли;
	
	Кабинеты_ = Новый Массив;
	Если ЗначениеЗаполнено(Кабинет_) Тогда
		Кабинеты_.Добавить(Кабинет_);	
	Иначе
		Кабинеты_ = МедицинскиеРабочиеМеста.ПолучитьКабинетыПриходящихВрачей(РабочееМесто);
	КонецЕсли;
	
	Если Кабинеты_.Количество() > 0 Тогда
		Если КонтрольЗанятости = Истина Тогда
			Результат_.Кабинет = РегистрыСведений.ТалоныМедицинскихКабинетов.НайтиСвободныйКабинет(
				Кабинеты_, Время, ЧислоТалонов, ДоляЗанятости
			);
			Результат_.ОК = ЗначениеЗаполнено(Результат_.Кабинет);
		Иначе
			Результат_.Кабинет = Кабинеты_[0];
			Результат_.ОК = Истина;
		КонецЕсли;
	Иначе
		Результат_.ОК = Истина;
	КонецЕсли;
	Возврат Результат_;
КонецФункции


////
 // Функция: СобратьИнформациюДляПерепланирования
 //		Используется в процедуре ПерепланироватьУслуги модуля ПланированиеУслугКлиент.
 //     Определяет каким образом запланированы услуги в сетке или легковесно и собирает необходимые данные
 //     для открытия соответственно сетки или заказа пациента.
 //     
 // Возврат:
 //   Структура, с данными для открытия сетки или заказа.
 ///
 &Вместо("СобратьИнформациюДляПерепланирования")
Функция СобратьИнформациюДляПерепланирования_Расширение(УИДыУслуг, УИДыПриемов = Неопределено) Экспорт
	Результат_ = Новый Структура(
		"Услуги, Приемы, ОткрыватьСетку, Сообщение, МедицинскаяКарта, Пациент, УКД_ИспользоватьСанаторнуюСетку", 
		Новый Массив, Новый Массив, Неопределено, "", Неопределено, Неопределено, Ложь
	);
	
	Запрос_ = Новый Запрос(
		"ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	РегистрСтатусыУслуг.УникальныйИдентификаторУслуги,
		|	РегистрСтатусыУслуг.Номенклатура,
		|	РегистрСтатусыУслуг.Заказ,
		|	РегистрСтатусыУслуг.СтатусУслуги,
		|	РегистрСтатусыУслуг.МедицинскаяКарта,
		|	РегистрСтатусыУслуг.Пациент,
		|	РегистрСтатусыУслуг.Соглашение,
		|	РегистрСтатусыУслуг.ИсточникФинансирования,
		|	РегистрСменныеЗадания.СпецификацияМедицинскойУслуги,
		|	РегистрСменныеЗадания.МедицинскоеРабочееМесто,
		|	РегистрСменныеЗадания.ЗапланированноеВремя,
		|	РегистрСменныеЗадания.ПланированиеНаСмену,
		|	РегистрСменныеЗадания.КлючСтрокиЭтапа,
		|	РегистрСменныеЗадания.ЧислоТалонов,
		|	РегистрСменныеЗадания.ДоляЗанятости,
		|	РегистрСменныеЗадания.ДатаВыполнения,
		|	РегистрСменныеЗадания.ДокументПланирования,
		|	РегистрСменныеЗадания.МедицинскийКабинет,
		|	ЛОЖЬ КАК ЧислоТалоновЗаданоПользователем
		|ИЗ
		|	РегистрСведений.СтатусыУслуг КАК РегистрСтатусыУслуг
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.СменныеЗадания КАК РегистрСменныеЗадания
		|		ПО РегистрСтатусыУслуг.УникальныйИдентификаторУслуги = РегистрСменныеЗадания.УникальныйИдентификаторУслуги
		|ГДЕ
		|	РегистрСтатусыУслуг.УникальныйИдентификаторУслуги В(&УИДыУслуг)"
	);
	Запрос_.УстановитьПараметр("УИДыУслуг", УИДыУслуг);
	РезультатЗапроса_ = Запрос_.Выполнить();
	
	Выгрузка_ = РезультатЗапроса_.Выгрузить();
	Для Каждого Услуга_ Из Выгрузка_ Цикл
		Если ЗначениеЗаполнено(Услуга_.ДатаВыполнения) Тогда
			Сообщение_ = СообщенияПользователю.Получить(
				"ОформлениеЗаказов_ЗапрещеноИзменятьПланированиеВыполненнойУслуги"
			);
			Результат_.Сообщение = Результат_.Сообщение + Сообщение_ + Символы.ПС;
		ИначеЕсли УправлениеЗаказамиУслугами.ЭтоУслугаСтационара(Услуга_.УникальныйИдентификаторУслуги) Тогда
			ПараметрыСообщения_ = Новый Структура("Номенклатура", Услуга_.Номенклатура);
			Сообщение_ = СообщенияПользователю.Получить(
				"ПланированиеУслуг_СтационарнаяУслугаПланируетсяВСменномЗадании", ПараметрыСообщения_
			);
			Результат_.Сообщение = Результат_.Сообщение + Сообщение_ + Символы.ПС;
		ИначеЕсли УправлениеЗаказамиУслугами.УслугаВыполняется(Услуга_.УникальныйИдентификаторУслуги) Тогда
			ПараметрыСообщения_ = Новый Структура("Номенклатура", Услуга_.Номенклатура);
			Сообщение_ = СообщенияПользователю.Получить(
				"ПланированиеУслуг_ЗапрещеноИзменятьПланированиеВыполняющейсяУслуги", ПараметрыСообщения_
			);
			Результат_.Сообщение = Результат_.Сообщение + Сообщение_ + Символы.ПС;
		ИначеЕсли Услуга_.СтатусУслуги = Перечисления.СтатусыУслуг.Назначена Тогда
			ПараметрыСообщения_ = Новый Структура("Номенклатура", Услуга_.Номенклатура);
			Сообщение_ = СообщенияПользователю.Получить(
				"ПланированиеУслуг_НазначеннаяУслугаПланируетсяПриФормированииЗаказа", ПараметрыСообщения_
			);
			Результат_.Сообщение = Результат_.Сообщение + Сообщение_ + Символы.ПС;
		Иначе
			ПланироватьВСетке_ = Ложь;
			Если ЗначениеЗаполнено(Услуга_.ДокументПланирования) И
				 ТипЗнч(Услуга_.ДокументПланирования) = Тип("ДокументСсылка.РезервированиеРабочихМест") 
			Тогда
				ПланироватьВСетке_ = Истина;
			КонецЕсли;
			Если ПланироватьВСетке_ = Ложь Тогда
				ТипПланирования_ = ОпределитьТипПланированияУслуги(
					Услуга_.Номенклатура, 
					?(Услуга_.СпецификацияМедицинскойУслуги = Null, Неопределено, ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(Услуга_.СпецификацияМедицинскойУслуги))
				);
				ПланироватьВСетке_ = ТипПланирования_.ВСетке;
			КонецЕсли;
			Результат_.ОткрыватьСетку = ?(
				Результат_.ОткрыватьСетку = Неопределено, ПланироватьВСетке_, Результат_.ОткрыватьСетку
			);
			Если Результат_.ОткрыватьСетку = ПланироватьВСетке_ Тогда
				Результат_.Услуги.Добавить(ОбщегоНазначения.СтрокаТаблицыЗначенийВСтруктуру(Услуга_));
			КонецЕсли;
			Результат_.МедицинскаяКарта = Услуга_.МедицинскаяКарта;
			Результат_.Пациент = Услуга_.Пациент;
			Результат_.ИспользоватьСанаторнуюСетку = ПолучитьФункциональнуюОпцию("УКД_ИспользоватьСанаторнуюСетку");
		КонецЕсли;	
	КонецЦикла;
	
	Если УИДыПриемов <> Неопределено Тогда
		Запрос_ = Новый Запрос(
			"ВЫБРАТЬ
			|	РегистрПриемы.УникальныйИдентификаторПриема,
			|	РегистрПриемы.Номенклатура,
			|	РегистрПриемы.ДокументЗаказа КАК Заказ,
			|	РегистрПриемы.ДокументПланирования,
			|	РегистрПриемы.МедицинскоеРабочееМесто,
			|	РегистрПриемы.МедицинскийКабинет,
			|	РегистрПриемы.ЗапланированноеВремя,
			|	РегистрПриемы.ЧислоТалонов,
			|	РегистрПриемы.ДоляЗанятости,
			|	РегистрПриемы.МедицинскаяКарта,
			|	РегистрПриемы.Пациент
			|ИЗ
			|	РегистрСведений.Приемы КАК РегистрПриемы
			|ГДЕ
			|	РегистрПриемы.УникальныйИдентификаторПриема В(&УИДыПриемов)
			|	И РегистрПриемы.ДатаВыполнения = ДАТАВРЕМЯ(1, 1, 1)
			|	И РегистрПриемы.ДокументОтмены = ЗНАЧЕНИЕ(Документ.ОтменаПриемов.ПустаяСсылка)"
		);	
		Запрос_.УстановитьПараметр("УИДыПриемов", УИДыПриемов);
		РезультатЗапроса_ = Запрос_.Выполнить();
		Выгрузка_ = РезультатЗапроса_.Выгрузить();
		Для Каждого Прием_ Из Выгрузка_ Цикл
			ПланироватьВСетке_ = Ложь;
			Если ЗначениеЗаполнено(Прием_.ДокументПланирования) И
				 ТипЗнч(Прием_.ДокументПланирования) = Тип("ДокументСсылка.РезервированиеРабочихМест") 
			Тогда
				ПланироватьВСетке_ = Истина;
			КонецЕсли;
			Если ПланироватьВСетке_ = Ложь Тогда
				ТипПланирования_ = ОпределитьТипПланированияПриема(Прием_.Номенклатура);
				ПланироватьВСетке_ = ТипПланирования_.ВСетке;
			КонецЕсли;
			Результат_.ОткрыватьСетку = ?(
				Результат_.ОткрыватьСетку = Неопределено, ПланироватьВСетке_, Результат_.ОткрыватьСетку
			);
			Если Результат_.ОткрыватьСетку = ПланироватьВСетке_ Тогда
				Результат_.Приемы.Добавить(ОбщегоНазначения.СтрокаТаблицыЗначенийВСтруктуру(Прием_));
			КонецЕсли;
			Результат_.МедицинскаяКарта = Прием_.МедицинскаяКарта;
			Результат_.Пациент = Прием_.Пациент;
			Результат_.ИспользоватьСанаторнуюСетку = ПолучитьФункциональнуюОпцию("УКД_ИспользоватьСанаторнуюСетку");
		КонецЦикла;
	КонецЕсли;
	Возврат Результат_;
КонецФункции

//ГТ+++
Процедура ПолучитьВсеУслугиПациента(Услуги) Экспорт
	//МедицинскаяКарта_ = Неопределено; 
	//Пациент_ = Неопределено; 
	//Регистратура.ОпределитьТекущийСеансПациента(МедицинскаяКарта_, Пациент_);
	//
	//// Доступные типы карт
	//СписокДоступныхТиповКарт_ = НастройкиКлиентСервер.Получить("ТипКарты");
	//
	//Запрос	= Новый Запрос();
	//ТекстЗапрос_ = 
	//"ВЫБРАТЬ
	//|	СтатусыУслуг.Номенклатура КАК Номенклатура,
	//|	СтатусыУслуг.УникальныйИдентификаторУслуги КАК УникальныйИдентификаторУслуги
	//|ИЗ
	//|	РегистрСведений.СтатусыУслуг КАК СтатусыУслуг
	//|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.Номенклатура КАК СправочникНоменклатура
	//|		ПО СтатусыУслуг.Номенклатура = СправочникНоменклатура.Ссылка
	//|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.МедицинскиеКарты КАК СправочникМедицинскиеКарты
	//|		ПО СтатусыУслуг.МедицинскаяКарта = СправочникМедицинскиеКарты.Ссылка
	//|ГДЕ
	//|	СтатусыУслуг.Пациент = &Пациент
	//|	И ВЫБОР КОГДА СтатусыУслуг.МедицинскаяКарта = ЗНАЧЕНИЕ(Справочник.МедицинскиеКарты.ПустаяСсылка)
	//|		ТОГДА ИСТИНА
	//|			КОГДА СправочникМедицинскиеКарты.ТипМедицинскойКарты В (&ДоступныеТипыМедКарт)
	//|		ТОГДА ИСТИНА
	//|			ИНАЧЕ ЛОЖЬ КОНЕЦ
	//|	И ВЫБОР
	//|			КОГДА СправочникНоменклатура.ХарактерМедицинскойУслуги В (&Характеры)
	//|				ТОГДА ИСТИНА
	//|			КОГДА НЕ СтатусыУслуг.Заказ = ЗНАЧЕНИЕ(Документ.ЗаказПациента.ПустаяСсылка)
	//|				ТОГДА ИСТИНА
	//|			ИНАЧЕ ЛОЖЬ
	//|		КОНЕЦ";
	//Запрос.Текст = ТекстЗапрос_;
	//Запрос.УстановитьПараметр("Пациент", Пациент_);
	//Запрос.УстановитьПараметр(
	//		"ДоступныеТипыМедКарт", СписокДоступныхТиповКарт_.ВыгрузитьЗначения()
	//	);
	//
	//Характеры_ = Новый Массив;
	//Характеры_.Добавить(ПредопределенноеЗначение("Перечисление.ХарактерМедицинскихУслуг.ПустаяСсылка"));
	//Характеры_.Добавить(ПредопределенноеЗначение("Перечисление.ХарактерМедицинскихУслуг.ЦИТО"));
	//Характеры_.Добавить(ПредопределенноеЗначение("Перечисление.ХарактерМедицинскихУслуг.Посещение"));
	//
	//Запрос.УстановитьПараметр(
	//	"Характеры", Характеры_
	//);

	//Выборка_ = Запрос.Выполнить().Выбрать();

	//Пока Выборка_.Следующий() Цикл
	//    Услуги.Добавить(Выборка_.УникальныйИдентификаторУслуги);
	//КонецЦикла;
КонецПроцедуры //ПолучитьВсеУслугиПациента
