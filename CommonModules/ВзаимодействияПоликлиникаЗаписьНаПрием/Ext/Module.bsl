﻿
// Заполнить соглашение услуги по умолчанию.
&Вместо("ЗаполнитьСоглашениеУслуги")
Процедура ЗаполнитьСоглашениеУслуги_Расширение(
	Взаимодействие, Заказ, Услуга, ДоступныеСоглашения, Пациент, Контакт, ДатаРождения
) Экспорт
	Соглашение_ = ДоступныеСоглашения[0].Соглашение;
	Если ДоступныеСоглашения[0].ПЛТ Тогда
		Для Каждого СтрСоглашение Из ДоступныеСоглашения Цикл
			Если СокрЛП(СтрСоглашение.Соглашение.Наименование) = "Коммерция" Тогда
				Соглашение_ =  СтрСоглашение.Соглашение;
				Прервать;
			КонецЕсли;	
		КонецЦикла;	
	КонецЕсли;
	Услуга.Соглашение = Соглашение_;	
	Услуга.ИсточникФинансирования = Соглашение_.ИсточникФинансирования;
	ОбновитьСоглашениеУслуги(
		Взаимодействие, Заказ, Услуга, ДоступныеСоглашения, Пациент, Контакт, ДатаРождения
	);
КонецПроцедуры
 

