﻿
////
 // Функция: ПолучитьКабинеты
 ///
Функция ПолучитьКабинеты(РабочееМесто, ПараметрыСетки)
	Возврат ПолучитьРабочееМесто(РабочееМесто, ПараметрыСетки).Кабинеты;
КонецФункции

////
 // Функция: ПолучитьСвободныйКабинет
 ///
Функция ПолучитьСвободныйКабинет(РабочееМесто, Талон, ПараметрыСетки) Экспорт
	Свободный_ = Неопределено;
	Для Каждого Кабинет_ Из ПолучитьКабинеты(РабочееМесто, ПараметрыСетки) Цикл
		Если ТалонКабинетаЗанят(Кабинет_, Талон, ПараметрыСетки) = Ложь Тогда
			Свободный_ = Кабинет_;
			Прервать;
		КонецЕсли;
	КонецЦикла;
	Возврат Свободный_;
КонецФункции

////
 // Функция: ТалонДоступен
 ///
Функция ТалонДоступен(Спецификация, РабочееМесто, Талон, ПараметрыСетки) Экспорт
	Доступен_ = Истина;
	Если ТалоныСвязанныхРабочихМестСвободны(РабочееМесто, Талон, ПараметрыСетки) = Ложь Тогда
		Доступен_ = Ложь;
	КонецЕсли;
	Если ТалонКабинетаДоступен(РабочееМесто, Талон, ПараметрыСетки) = Ложь Тогда
		Доступен_ = Ложь;
	КонецЕсли;
	Возврат Доступен_;
КонецФункции


////
 // Функция: ТалонКабинетаДоступен
 ///
Функция ТалонКабинетаДоступен(РабочееМесто, Талон, ПараметрыСетки) Экспорт
	Доступен_ = Истина;
	Если ПолучитьКабинеты(РабочееМесто, ПараметрыСетки).Количество() > 0 Тогда
		Доступен_ = ПолучитьСвободныйКабинет(РабочееМесто, Талон, ПараметрыСетки) <> Неопределено;
	КонецЕсли;
	Возврат Доступен_;
КонецФункции

////
 // Функция: ТалонКабинетаЗанят
 ///
Функция ТалонКабинетаЗанят(Кабинет, Талон, ПараметрыСетки)
	Талоны_ = ПолучитьТалоныКабинета(Кабинет, ПараметрыСетки);
	Занят_ = Талоны_.Получить(Талон);
	Возврат ?(Занят_ <> Неопределено, Занят_, Ложь);	
КонецФункции
