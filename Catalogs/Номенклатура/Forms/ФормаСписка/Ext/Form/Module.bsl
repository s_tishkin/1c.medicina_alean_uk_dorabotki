﻿
&НаСервере
Процедура УКД_ПриСозданииНаСервереПосле(Отказ, СтандартнаяОбработка)
	//ГТ
	ДоступныеПодразделения_ = РаботаССоглашениями.ПолучитьДоступныеПользователюПодразделения().ВыгрузитьЗначения();
	
	Если ЗначениеЗаполнено(ДоступныеПодразделения_) Тогда
		ДоступныеПодразделения_.Добавить(Справочники.СтруктураПредприятия.ПустаяСсылка());
		КомпоновкаДанныхКлиентСервер.ДобавитьОтбор(Список.Отбор, "Подразделение", ДоступныеПодразделения_, ВидСравненияКомпоновкиДанных.ВСпискеПоИерархии, Истина);
		//Список.Параметры.УстановитьЗначениеПараметра("ДоступныеПодразделения",ДоступныеПодразделения_);	
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
&ИзменениеИКонтроль("СоздатьНоменклатуру")
Процедура УКД_СоздатьНоменклатуру(Команда)
	ТипНоменклатуры_ = Неопределено;
	Если Команда = ЭтотОбъект.Команды.СоздатьМедицинскуюУслугу Тогда
		ТипНоменклатуры_ = ПредопределенноеЗначение("Справочник.ТипыНоменклатуры.МедицинскаяУслуга");	
	ИначеЕсли Команда = ЭтотОбъект.Команды.СоздатьМедицинскуюПрограмму Тогда
		ТипНоменклатуры_ = ПредопределенноеЗначение("Справочник.ТипыНоменклатуры.МедицинскаяПрограмма");	
	ИначеЕсли Команда = ЭтотОбъект.Команды.СоздатьСтандартМедицинскойПомощи Тогда
		ТипНоменклатуры_ = ПредопределенноеЗначение(
		"Справочник.ТипыНоменклатуры.СтандартМедицинскойПомощи"
		);	
	ИначеЕсли Команда = ЭтотОбъект.Команды.СоздатьГотовуюЛекарственнуюФорму Тогда
		ТипНоменклатуры_ = ПредопределенноеЗначение(
		"Справочник.ТипыНоменклатуры.ГотоваяЛекарственнаяФорма"
		);	
	ИначеЕсли Команда = ЭтотОбъект.Команды.СоздатьИзготавливаемуюЛекарственнуюФорму Тогда
		ТипНоменклатуры_ = ПредопределенноеЗначение(
		"Справочник.ТипыНоменклатуры.ИзготавливаемаяЛекарственнаяФорма"
		);	
	ИначеЕсли Команда = ЭтотОбъект.Команды.СоздатьУслугу Тогда
		ТипНоменклатуры_ = ПредопределенноеЗначение("Справочник.ТипыНоменклатуры.Услуга");	
	ИначеЕсли Команда = ЭтотОбъект.Команды.СоздатьТовар Тогда
		ТипНоменклатуры_ = ПредопределенноеЗначение("Справочник.ТипыНоменклатуры.Товар");	
	ИначеЕсли Команда = ЭтотОбъект.Команды.СоздатьПрием Тогда
		ТипНоменклатуры_ = ПредопределенноеЗначение("Справочник.ТипыНоменклатуры.Прием");
#Вставка
	ИначеЕсли Команда = ЭтотОбъект.Команды.СоздатьСанКур Тогда
		ТипНоменклатуры_ = СанаторноКурортноеЛечение();
#КонецВставки
	КонецЕсли;

	__ПРОВЕРКА__(ЗначениеЗаполнено(ТипНоменклатуры_), "Не указан тип номенклатуры");

	ЗначенияЗаполнения_ = Новый Структура(
	"ТипНоменклатуры, Родитель", ТипНоменклатуры_, Элементы.Список.ТекущийРодитель
	);
	Параметры_ = Новый Структура("ЗначенияЗаполнения", ЗначенияЗаполнения_); 
	Режим_ = РежимОткрытияОкнаФормы.БлокироватьВесьИнтерфейс;	
	ОткрытьФорму("Справочник.Номенклатура.ФормаОбъекта", Параметры_,,,,,, Режим_);
КонецПроцедуры

&НаСервереБезКонтекста
Функция СанаторноКурортноеЛечение()
	Возврат Справочники.ТипыНоменклатуры.СанаторноКурортноеЛечение();
КонецФункции
