﻿
&НаКлиенте
Процедура Отмена(Команда)
	Закрыть();
КонецПроцедуры

&НаКлиенте
Процедура ПолучитьГостя(Команда)
	ОписаниеОшибки_ = "";
	Если Не ЗначениеВвода = 0 Тогда
		ПолучитьГостяНаСервере(ЗначениеВвода, ОписаниеОшибки_);
		Если не ПустаяСтрока(ОписаниеОшибки_) Тогда
			ПоказатьПредупреждение(,ОписаниеОшибки_);
		КонецЕсли;	
	КонецЕсли;	
КонецПроцедуры

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("Подразделение") Тогда
		Подразделение =  Параметры.Подразделение;
	КонецЕсли;	
	Если Подразделение.Пустая() Тогда
		РМ_ = ПараметрыСеанса.РабочееМестоКлиента;
		Подразделение = РМ_.Подразделение;
	КонецЕсли;	
	Если Параметры.Свойство("СуммаЗаказа") Тогда
		СуммаЗаказа =  Параметры.СуммаЗаказа;
	КонецЕсли;
	Если Параметры.Свойство("ПараметрЗапроса",ПараметрЗапроса) Тогда
		ПараметрЗапроса =  Параметры.ПараметрЗапроса;
	КонецЕсли;	
	Параметры.Свойство("КодКлиента",КлиентКод);
	
	Если Параметры.Свойство("КартаГостя",Идентификатор) Тогда
		ПолучитьГостяНаСервере(Идентификатор, "");
	ИначеЕсли ПараметрЗапроса = "Комната" и Параметры.Свойство("ЗначениеВвода",НомерКомнаты) И ЗначениеЗаполнено(НомерКомнаты) Тогда
		ПолучитьГостяНаСервере(НомерКомнаты, "");
	ИначеЕсли ПараметрЗапроса = "Фолио" И Параметры.Свойство("ЗначениеВвода",Фолио) И ЗначениеЗаполнено(Фолио) Тогда
		ПолучитьГостяНаСервере(Фолио, "");
	КонецЕсли;	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	Если Не ЗначениеЗаполнено(ЭтаФорма.Клиент) И СписокКлиентов.Количество() = 0 Тогда
		УправлениеВидимостью();
	КонецЕсли;
	Если Подразделение.Пустая() Тогда
		РабочееМесто_ 	= МенеджерОборудованияКлиентПовтИсп.ПолучитьРабочееМестоКлиента();
		Подразделение 	= ОбщиеМеханизмы.ЗначениеРеквизитаОбъекта(РабочееМесто_,"Подразделение");
	КонецЕсли;	
КонецПроцедуры

&НаСервере
Процедура УправлениеВидимостью(НомерСтраницы=0)
	Если Не НомерСтраницы=0 Тогда
		Если НомерСтраницы = 1 Тогда  //Выбор Комнаты
			
			Заголовок = "Введите номер комнаты";
			
			Элементы.грВводНомера.Видимость 		= Истина;
			Элементы.ГрВыборКлиента.Видимость 		= Ложь;
			Элементы.грПодтверждение.Видимость 		= Ложь;	
			Элементы.грВыборСпособаОплаты.Видимость = Ложь;
			
		ИначеЕсли НомерСтраницы= 2 Тогда //Выбор Фолио	
			
			Заголовок = "Введите номер фолио";
			
			Элементы.грВводНомера.Видимость 		= Истина;
			Элементы.ГрВыборКлиента.Видимость 		= Ложь;
			Элементы.грПодтверждение.Видимость 		= Ложь;
			Элементы.грВыборСпособаОплаты.Видимость	= Ложь;
			
		ИначеЕсли НомерСтраницы= 3 Тогда //Список гостей номера	
			Заголовок = "Веберите гостя из списка";
			Элементы.грВводНомера.Видимость 		= Ложь;
			Элементы.ГрВыборКлиента.Видимость 		= Истина;
			Элементы.грПодтверждение.Видимость 		= Ложь;
			Элементы.грВыборСпособаОплаты.Видимость	= Ложь;
			
		ИначеЕсли НомерСтраницы= 4 Тогда //Данные гостя
			Заголовок = Клиент;
			Элементы.грВводНомера.Видимость 		= Ложь;
			Элементы.ГрВыборКлиента.Видимость 		= Ложь;
			Элементы.грПодтверждение.Видимость 		= Истина;
			Элементы.грВыборСпособаОплаты.Видимость	= Ложь;
			Элементы.грОплата.Видимость 			= Не (ДоступныеСредства >= СуммаЗаказа);
			
			Элементы.ОК.Видимость = Истина;
			Если Не (ДоступныеСредства >= СуммаЗаказа) Или Заблокирована Или Выехал Тогда
				
				Элементы.ОК.Видимость 					= Ложь;
				
			КонецЕсли;
				
		ИначеЕсли НомерСтраницы = 5 Тогда //Выбор способа оплаты
			Заголовок = "Выберите способ оплаты или считайте карту гостя";
			
			Элементы.грВводНомера.Видимость 		= Ложь;
			Элементы.ГрВыборКлиента.Видимость 		= Ложь;
			Элементы.грПодтверждение.Видимость 		= Ложь;
            Элементы.грВыборСпособаОплаты.Видимость	= Истина;
			
		КонецЕсли;	
	Иначе	
		Если Не ПустаяСтрока(ПараметрЗапроса)  Тогда
			Если ПараметрЗапроса = "Комната" Тогда
				
				Заголовок = "Введите номер комнаты";
				
				Элементы.грВводНомера.Видимость 		= Истина;
				Элементы.ГрВыборКлиента.Видимость 		= Ложь;
				Элементы.грПодтверждение.Видимость 		= Ложь;
				Элементы.грВыборСпособаОплаты.Видимость	= Ложь;
				
			ИначеЕсли ПараметрЗапроса = "Фолио"	Тогда
				
				Заголовок = "Введите номер фолио";
				
				Элементы.грВводНомера.Видимость 		= Истина;
				Элементы.ГрВыборКлиента.Видимость 		= Ложь;
				Элементы.грПодтверждение.Видимость 		= Ложь;
				Элементы.грВыборСпособаОплаты.Видимость	= Ложь;
			Иначе
				Заголовок = Клиент;
				Элементы.грВводНомера.Видимость 		= Ложь;
				Элементы.ГрВыборКлиента.Видимость 		= Ложь;
				Элементы.грПодтверждение.Видимость 		= Истина;
				Элементы.грВыборСпособаОплаты.Видимость	= Ложь;
			КонецЕсли;	
		Иначе
			Заголовок = "Выберите способ оплаты или считайте карту гостя";
			
			Элементы.грВводНомера.Видимость 		= Ложь;
			Элементы.ГрВыборКлиента.Видимость 		= Ложь;
			Элементы.грПодтверждение.Видимость 		= Ложь;
            Элементы.грВыборСпособаОплаты.Видимость	= Истина;
		КонецЕсли;	
	КонецЕсли;
	Элементы.грВыехал.Видимость = Выехал;
	Если Выехал Или Заблокирована Тогда
		Элементы.ОК.Видимость = Ложь;
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура ПолучитьГостяНаСервере(ЗначениеВвода, пОписаниеОшибки_)
	Если ПараметрЗапроса = "Комната" Тогда
		Результат = УКД_Румба.ВыбратьКлиентаПоНомеруКомнаты(ЗначениеВвода,Подразделение, СписокКлиентов, пОписаниеОшибки_);
		Если  Результат и ПустаяСтрока(пОписаниеОшибки_) Тогда
			Если СписокКлиентов.Количество() = 1 Тогда
				КлиентОтеля = УКД_Румба.ПолучитьКлиентаГостиницы(СписокКлиентов[0].Значение);
				УКД_Румба.ДетальнаяИнформацияКлиента(КлиентОтеля,Подразделение.УКД_СтрокаПодключения, пОписаниеОшибки_);
				Если ПустаяСтрока(пОписаниеОшибки_) Тогда
					ЗаполнитьЗначенияСвойств(ЭтаФорма,КлиентОтеля);
					УправлениеВидимостью(4);
				КонецЕсли;
			Иначе
				Если СписокКлиентов.Количество() > 1 И Не ПустаяСтрока(КлиентКод) Тогда
					КлиентНайден = ложь;
					
					Для Каждого СтрКлиент Из СписокКлиентов Цикл
						
						Если Найти(СтрКлиент.Значение,КлиентКод)>0 Тогда
							
							КлиентОтеля = УКД_Румба.ПолучитьКлиентаГостиницы(СтрКлиент.Значение);
							УКД_Румба.ДетальнаяИнформацияКлиента(КлиентОтеля,Подразделение.УКД_СтрокаПодключения, пОписаниеОшибки_);
							
							Если ПустаяСтрока(пОписаниеОшибки_) Тогда
								ЗаполнитьЗначенияСвойств(ЭтаФорма,КлиентОтеля);
								
								УправлениеВидимостью(4);
								
								КлиентНайден = Истина;
								
								Прервать;
							КонецЕсли;
						КонецЕсли;	
					КонецЦикла;	
					
					Если Не КлиентНайден Тогда
						УправлениеВидимостью(3);
					КонецЕсли;
				Иначе	
					УправлениеВидимостью(3);
				КонецЕсли;
			КонецЕсли;	
		Иначе
			ЗаписьЖурналаРегистрации("ОшибкаОбмена.ПолучитьФотоКлиента",УровеньЖурналаРегистрации.Предупреждение,,,пОписаниеОшибки_);
			Возврат;
		КонецЕсли;	
	ИначеЕсли ПараметрЗапроса = "Фолио" Тогда
		КлиентОтеля = Неопределено;
		Результат = УКД_Румба.ВыбратьКлиентаПоФолио(ЗначениеВвода,Подразделение, КлиентОтеля, пОписаниеОшибки_);
		Если Результат и ПустаяСтрока(пОписаниеОшибки_) Тогда
			Если Не КлиентОтеля = Неопределено Тогда
				УКД_Румба.ДетальнаяИнформацияКлиента(КлиентОтеля,Подразделение.УКД_СтрокаПодключения, пОписаниеОшибки_);
				Если ПустаяСтрока(пОписаниеОшибки_) Тогда
					ЗаполнитьЗначенияСвойств(ЭтаФорма,КлиентОтеля);
					УправлениеВидимостью(4);
				КонецЕсли;
			Иначе
				УправлениеВидимостью(3);
			КонецЕсли;	
		Иначе
			ЗаписьЖурналаРегистрации("ОшибкаОбмена.ПолучитьКлиента",УровеньЖурналаРегистрации.Ошибка,,,пОписаниеОшибки_);
			Если Найти(пОписаниеОшибки_,"Лицевой счет не найден по номеру!")>0 Тогда
				пОписаниеОшибки_ = "Лицевой счет не найден!"+Символы.ПС+"Проверьте правильность ввода!";
			КонецЕсли;	
			Если Найти(пОписаниеОшибки_,"Лицевой счет закрыт!")>0 Тогда
				пОписаниеОшибки_ = "Лицевой счет закрыт!";
			КонецЕсли;
			Возврат;
		КонецЕсли;	
	ИначеЕсли ПараметрЗапроса = "КартаГостя" Тогда	
		Результат = УКД_Румба.ВыбратьКлиентаПоКартеГостя(ЗначениеВвода,Подразделение,КлиентОтеля,пОписаниеОшибки_);
		Если Результат и ПустаяСтрока(пОписаниеОшибки_) Тогда
			Если Не КлиентОтеля = Неопределено Тогда
				УКД_Румба.ДетальнаяИнформацияКлиента(КлиентОтеля,Подразделение.УКД_СтрокаПодключения, пОписаниеОшибки_);
				Если ПустаяСтрока(пОписаниеОшибки_) Тогда
					ЗаполнитьЗначенияСвойств(ЭтаФорма,КлиентОтеля);
					УправлениеВидимостью(4);
				КонецЕсли;
			Иначе
				УправлениеВидимостью(3);
			КонецЕсли;	
		Иначе
			ЗаписьЖурналаРегистрации("ОшибкаОбмена.ПолучитьКлиента",УровеньЖурналаРегистрации.Ошибка,,,пОписаниеОшибки_);
			Возврат;
		КонецЕсли;	
	КонецЕсли;	
КонецПроцедуры // ПолучитьГостяНаСервере()

&НаКлиенте
Процедура СписокКлиентовВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	ВыбратьКлиентаНаСервере(ВыбраннаяСтрока);
КонецПроцедуры

&НаСервере
Процедура ВыбратьКлиентаНаСервере(пВыбраннаяСтрока)
	ОписаниеОшибки_="";
	текСтрока = СписокКлиентов.НайтиПоИдентификатору(пВыбраннаяСтрока);
	КлиентОтеля = УКД_Румба.ПолучитьКлиентаГостиницы(текСтрока.Значение);
	УКД_Румба.ДетальнаяИнформацияКлиента(КлиентОтеля,Подразделение.УКД_СтрокаПодключения, ОписаниеОшибки_);
	Если ПустаяСтрока(ОписаниеОшибки_) Тогда
		ЗаполнитьЗначенияСвойств(ЭтаФорма,КлиентОтеля);
		УправлениеВидимостью(4);
	Иначе
		Сообщить(ОписаниеОшибки_);
		ЗаписьЖурналаРегистрации("ОшибкаОбмена.ПолучитьФотоКлиента",УровеньЖурналаРегистрации.Предупреждение,,,ОписаниеОшибки_);
	КонецЕсли;
КонецПроцедуры


&НаКлиенте
Процедура ВыбратьКлиента(Команда)
	ВыбраннаяСтрока_ = Элементы.СписокКлиентов.ТекущаяСтрока;
	ВыбратьКлиентаНаСервере(ВыбраннаяСтрока_);
КонецПроцедуры


&НаКлиенте
Процедура ОК(Команда)
	ПараметрыЗакрытия = Новый Структура;
	ПараметрыЗакрытия.Вставить("Комната"		,НомерКомнаты);
	ПараметрыЗакрытия.Вставить("КлиентКод"		,КлиентКод);
	ПараметрыЗакрытия.Вставить("Фолио"			,Фолио);
	ПараметрыЗакрытия.Вставить("Идентификатор"	,Идентификатор);
	ПараметрыЗакрытия.Вставить("СпособЗакрытия"	,ПараметрЗапроса);
	ПараметрыЗакрытия.Вставить("Подразделение"	,Подразделение);
	ПараметрыЗакрытия.Вставить("Плательщик"		,Клиент);
	
	Закрыть(ПараметрыЗакрытия);
КонецПроцедуры
 

&НаКлиенте
Процедура ЗакрытьНаНомер(Команда)
	ПараметрЗапроса = "Комната";
	УправлениеВидимостью(1);
КонецПроцедуры


&НаКлиенте
Процедура ЗакрытьНаФолио(Команда)
	ПараметрЗапроса = "Фолио";
	УправлениеВидимостью(2);
КонецПроцедуры

&НаКлиенте
Процедура Цифра1(Команда)
	ЗначениеВвода = ЗначениеВвода+"1";
КонецПроцедуры

&НаКлиенте
Процедура Цифра2(Команда)
	ЗначениеВвода = ЗначениеВвода+"2";
КонецПроцедуры

&НаКлиенте
Процедура Цифра3(Команда)
	ЗначениеВвода = ЗначениеВвода+"3";
КонецПроцедуры

&НаКлиенте
Процедура Цифра4(Команда)
	ЗначениеВвода = ЗначениеВвода+"4";
КонецПроцедуры

&НаКлиенте
Процедура Цифра5(Команда)
	ЗначениеВвода = ЗначениеВвода+"5";
КонецПроцедуры

&НаКлиенте
Процедура Цифра6(Команда)
	ЗначениеВвода = ЗначениеВвода+"6";
КонецПроцедуры

&НаКлиенте
Процедура Цифра7(Команда)
	ЗначениеВвода = ЗначениеВвода+"7";
КонецПроцедуры

&НаКлиенте
Процедура Цифра8(Команда)
	ЗначениеВвода = ЗначениеВвода+"8";
КонецПроцедуры

&НаКлиенте
Процедура Цифра9(Команда)
	ЗначениеВвода = ЗначениеВвода+"9";
КонецПроцедуры

&НаКлиенте
Процедура Цифра0(Команда)
	ЗначениеВвода = ЗначениеВвода+"0";
КонецПроцедуры

&НаКлиенте
Процедура Цифра00(Команда)
	ЗначениеВвода = ЗначениеВвода+"00";
КонецПроцедуры

&НаКлиенте
Процедура КомандаС(Команда)
	ЗначениеВвода = Лев(ЗначениеВвода,СтрДлина(ЗначениеВвода)-1);
КонецПроцедуры

&НаКлиенте
Процедура ДругоеФолио(Команда)
	НомерСтраницы = 0;
	ПараметрЗапроса = "Фолио";
	УправлениеВидимостью();
КонецПроцедуры
