﻿
&НаСервереБезКонтекста
&Вместо("ПолучитьУслугиМедпрограммДляОтмены")
Функция ПолучитьУслугиМедпрограммДляОтмены_Расширение(УИДыМедпрограмм)
	Услуги_ = ПолучитьУслугиМедпрограмм(УИДыМедпрограмм);
	Запрос_ = Новый Запрос(
		"ВЫБРАТЬ
		|	СтатусыУслуг.УникальныйИдентификаторУслуги КАК УИДУслуги
		|ИЗ
		|	РегистрСведений.СтатусыУслуг КАК СтатусыУслуг
		|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.ЗаказПациента.МедицинскиеУслуги КАК ЗаказПациентаМедицинскиеУслуги
		|		ПО СтатусыУслуг.Заказ = ЗаказПациентаМедицинскиеУслуги.Ссылка
		|			И СтатусыУслуг.УникальныйИдентификаторУслуги = ЗаказПациентаМедицинскиеУслуги.УникальныйИдентификаторУслуги
		|ГДЕ
		|	СтатусыУслуг.УникальныйИдентификаторУслуги В(&Услуги)
		|	И (ВЫБОР
		|				КОГДА СтатусыУслуг.НаОплату = ИСТИНА
		|					ТОГДА ИСТИНА
		|				ИНАЧЕ НЕ СтатусыУслуг.ИсточникФинансирования В ИЕРАРХИИ (ЗНАЧЕНИЕ(Справочник.ИсточникиФинансирования.ПЛТ))
		|			КОНЕЦ
		|			ИЛИ ЗаказПациентаМедицинскиеУслуги.Сумма = 0)
		|	И СтатусыУслуг.СтатусУслуги В (ЗНАЧЕНИЕ(Перечисление.СтатусыУслуг.Заказана), ЗНАЧЕНИЕ(Перечисление.СтатусыУслуг.НаВыполнение))"
	);
	Запрос_.УстановитьПараметр("Услуги", Услуги_);
	Результат_ = Запрос_.Выполнить();
	Возврат ?(Результат_.Пустой(), Новый Массив, Результат_.Выгрузить().ВыгрузитьКолонку("УИДУслуги"));
КонецФункции


&НаСервере
Функция ЭтоГруппа(пЭлемент)

	Возврат пЭлемент.ЭтоГруппа;

КонецФункции // ЭтоГруппа()


&НаКлиенте
&Вместо("ЗаказатьМедпрограммы")
Процедура ЗаказатьМедпрограммы_Расширение(Строки)
	Медпрограммы_ = Новый Массив;
	Для Каждого Медпрограмма_ Из Строки Цикл
		Если ЭтоГруппа(Медпрограмма_) Тогда
			Продолжить;
		КонецЕсли;	
		Строка_ = Новый Структура("УИДМедпрограммы, Медпрограмма, Соглашение");
		Строка_.УИДМедпрограммы = Новый УникальныйИдентификатор;
		Строка_.Медпрограмма = Медпрограмма_;
		Строка_.Соглашение = ЭтотОбъект.Соглашение;
		Медпрограммы_.Добавить(Строка_);		
	КонецЦикла;
	
	Заказ_ = СоздатьЗаказМедпрограмм(ЭтотОбъект.МедицинскаяКарта, ЭтотОбъект.Пациент, Медпрограммы_);
	Если ЗначениеЗаполнено(Заказ_) Тогда
		Строки.Очистить();
		ОбновитьТаблицуДанных();
		Элементы.Медпрограммы.Обновить();
	КонецЕсли;
	
	Оповестить(ОповещенияФорм.ПриИзмененииПланированияУслуг());

КонецПроцедуры


&НаКлиенте
Процедура Обновить(Команда)
	ОбновитьТаблицуДанных();
	Элементы.Медпрограммы.Обновить();
КонецПроцедуры

&НаСервере
&Вместо("ОбновитьТаблицуДанных")
Процедура ОбновитьТаблицуДанных_Расширение()
	ИспользоватьВредность_ = ПолучитьФункциональнуюОпцию("ИспользоватьВредныеФакторы")
			И ЭтотОбъект.МедицинскаяКарта.ТипМедицинскойКарты.ИспользоватьВредныеФакторы;
	Элементы.ГруппаДатаОсмотра.Видимость = ИспользоватьВредность_;
	Элементы.ЗаказанныеМедпрограммыПереодичностьОсмотра.Видимость = ИспользоватьВредность_;
	Элементы.ЗаказанныеМедпрограммыРезультатОсмотра.Видимость = ИспользоватьВредность_;
	Элементы.ЗаказанныеМедпрограммыВредныйФактор.Видимость = ИспользоватьВредность_;
	Если ИспользоватьВредность_ Тогда
		ТекстЗапроса = 
			"ВЫБРАТЬ 
			|	ЗаказанныеПрограммы.Регистратор,
			|	ЗаказанныеПрограммы.НомерСтроки,
			|	ЗаказанныеПрограммы.Активность,
			|	ЗаказанныеПрограммы.УИДМедпрограммы,
			|	ЗаказанныеПрограммы.МедицинскаяКарта,
			|	ЗаказанныеПрограммы.Пациент,
			|	ЗаказанныеПрограммы.Состояние,
			|	ЗаказанныеПрограммы.Медпрограмма,
			|	ЗаказанныеПрограммы.ДатаЗаказа,
			|	ЗаказанныеПрограммы.ДатаЗакрытия,
			|	ЗаказанныеПрограммы.Регистратор КАК ЗаказМП,
			|	Графики.ДатаОсмотра,
			|	Графики.РезультатОсмотра,
			|	Графики.ВредныйФактор,
			|	ВредныеФакторы.Периодичность КАК ПериодичностьОсмотра,
			|	ЗаказанныеПрограммы.ПредварительныйОсмотр
			|ПОМЕСТИТЬ втГрафикиПрофосмотров	
			|ИЗ 
			|	РегистрСведений.ГрафикиПрофОсмотровПациентов КАК Графики
			|	ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ЗаказанныеМедицинскиеПрограммы КАК ЗаказанныеПрограммы
			|	ПО ЗаказанныеПрограммы.УИДМедпрограммы = Графики.УИДМедпрограммы
			|	ЛЕВОЕ СОЕДИНЕНИЕ Справочник.ВредныеФакторы КАК ВредныеФакторы
			|	ПО Графики.ВредныйФактор = ВредныеФакторы.Ссылка
			|ГДЕ 
			|	Графики.Пациент = &Пациент
			|ОБЪЕДИНИТЬ ВСЕ
			|
			|ВЫБРАТЬ 
			|	ЗаказанныеПрограммы.Регистратор,
			|	ЗаказанныеПрограммы.НомерСтроки,
			|	ЗаказанныеПрограммы.Активность,
			|	ЗаказанныеПрограммы.УИДМедпрограммы,
			|	ЗаказанныеПрограммы.МедицинскаяКарта,
			|	ЗаказанныеПрограммы.Пациент,
			|	ЗаказанныеПрограммы.Состояние,
			|	ЗаказанныеПрограммы.Медпрограмма,
			|	ЗаказанныеПрограммы.ДатаЗаказа,
			|	ЗаказанныеПрограммы.ДатаЗакрытия,
			|	ЗаказанныеПрограммы.Регистратор КАК ЗаказМП,
			|	0,
			|	0,
			|	0,
			|	0,
			|	ЗаказанныеПрограммы.ПредварительныйОсмотр
			|	
			|ИЗ 
			|	РегистрСведений.ЗаказанныеМедицинскиеПрограммы КАК ЗаказанныеПрограммы
			|	ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ГрафикиПрофОсмотровПациентов КАК Графики
			|	ПО ЗаказанныеПрограммы.УИДМедпрограммы = Графики.УИДМедпрограммы
			|	
			|ГДЕ 
			|	ЗаказанныеПрограммы.Пациент = &Пациент
			|	И ЗаказанныеПрограммы.МедицинскаяКарта = &МедицинскаяКарта
			|	И Графики.ДатаОсмотра ЕСТЬ NULL
			|;
			|ВЫБРАТЬ
			|	График.Регистратор,
			|	График.НомерСтроки,
			|	График.Активность,
			|	График.УИДМедпрограммы,
			|	График.МедицинскаяКарта,
			|	График.Пациент,
			|	График.Состояние,
			|	График.Медпрограмма,
			|	График.ДатаЗаказа,
			|	График.ДатаЗакрытия,
			|	График.ДатаОсмотра,
			|	График.РезультатОсмотра,
			|	График.ВредныйФактор,
			|	График.ПериодичностьОсмотра,
			|	График.ЗаказМП,
			|	График.ПредварительныйОсмотр
			|ИЗ втГрафикиПрофосмотров КАК График
			|УПОРЯДОЧИТЬ ПО График.ДатаОсмотра УБЫВ, График.ДатаЗаказа УБЫВ";
		Элементы.ЗаказанныеМедпрограммыДатаДеньНедели.Формат =  "ДФ = ддд";
	Иначе
		ТекстЗапроса = 
			"ВЫБРАТЬ
			|	РегистрСведенийЗаказанныеМедицинскиеПрограммы.Регистратор КАК ЗаказМП,
			|	РегистрСведенийЗаказанныеМедицинскиеПрограммы.НомерСтроки,
			|	РегистрСведенийЗаказанныеМедицинскиеПрограммы.Активность,
			|	РегистрСведенийЗаказанныеМедицинскиеПрограммы.УИДМедпрограммы,
			|	РегистрСведенийЗаказанныеМедицинскиеПрограммы.МедицинскаяКарта,
			|	РегистрСведенийЗаказанныеМедицинскиеПрограммы.Пациент,
			|	РегистрСведенийЗаказанныеМедицинскиеПрограммы.Состояние,
			|	РегистрСведенийЗаказанныеМедицинскиеПрограммы.Медпрограмма,
			|	РегистрСведенийЗаказанныеМедицинскиеПрограммы.ДатаЗаказа,
			|	РегистрСведенийЗаказанныеМедицинскиеПрограммы.ДатаЗакрытия
			|ИЗ
			|	РегистрСведений.ЗаказанныеМедицинскиеПрограммы КАК РегистрСведенийЗаказанныеМедицинскиеПрограммы
			|ГДЕ
			|	РегистрСведенийЗаказанныеМедицинскиеПрограммы.Пациент = &Пациент
			|	И РегистрСведенийЗаказанныеМедицинскиеПрограммы.МедицинскаяКарта = &МедицинскаяКарта
			|
			|УПОРЯДОЧИТЬ ПО
			|	РегистрСведенийЗаказанныеМедицинскиеПрограммы.ДатаЗаказа УБЫВ";
	КонецЕсли;
	Запрос_ = Новый Запрос(ТекстЗапроса);
	Запрос_.УстановитьПараметр("Пациент", ЭтотОбъект.Пациент);
	Запрос_.УстановитьПараметр("МедицинскаяКарта", ЭтотОбъект.МедицинскаяКарта);
	Результат_ = Запрос_.Выполнить().Выгрузить();
	ЗаказанныеМедпрограммы.Загрузить(Результат_);
КонецПроцедуры


&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	Если ИмяСобытия = "ОбновлениеМедПрограмм" Тогда
		ОбновитьТаблицуДанных();
		Элементы.Медпрограммы.Обновить();
	КонецЕсли;	
КонецПроцедуры


&НаКлиенте
Процедура ОткрытьДокумент(Команда)
	Строка_ = Элементы.ЗаказанныеМедпрограммы.ТекущиеДанные;
	Если Строка_ <> Неопределено И ЗначениеЗаполнено(Строка_.ЗаказМП) Тогда
		ДокМедПрограммы = Строка_.ЗаказМП;
		Если ДокМедПрограммы <> Неопределено Тогда
			Параметры_ = Новый Структура("Ключ", ДокМедПрограммы);
			ОткрытьФорму("Документ.ЗаказМедицинскихПрограмм.ФормаОбъекта", Параметры_);
		КонецЕсли;
	КонецЕсли;
КонецПроцедуры


&НаКлиенте
&Вместо("ОтменитьМедпрограммы")
Процедура ОтменитьМедпрограммы_Расширение(Команда)
	
	УИДыМедпрограмм_ = Новый Массив;
	Медпрограммы_ = Новый Массив;
	
	Отменена_ = ПредопределенноеЗначение("Перечисление.СтатусыУслуг.Отменена");
	Для Каждого Строка_ Из Элементы.ЗаказанныеМедпрограммы.ВыделенныеСтроки Цикл
		ЗаказаннаяМедпрограммма_ = Элементы.ЗаказанныеМедпрограммы.ДанныеСтроки(Строка_);
		Если Не ЗаказаннаяМедпрограммма_.Состояние = Отменена_ Тогда
			Медпрограмма_ = Новый Структура("УИДМедпрограммы, Медпрограмма");
			ЗаполнитьЗначенияСвойств(Медпрограмма_, ЗаказаннаяМедпрограммма_);
			УИДыМедпрограмм_.Добавить(ЗаказаннаяМедпрограммма_.УИДМедпрограммы);
			Медпрограммы_.Добавить(Медпрограмма_);
		КонецЕсли;
	КонецЦикла;
	
	Если Медпрограммы_.Количество() > 0 Тогда
		Услуги_ = ПолучитьУслугиМедпрограммДляОтмены(УИДыМедпрограмм_);
		Если Услуги_.Количество() > 0 Тогда
			СписокУслуг_ = Новый СписокЗначений;
			СписокУслуг_.ЗагрузитьЗначения(Услуги_);
			ПараметрыФормы_ = Новый Структура("СписокУид", СписокУслуг_);
			ПараметрыОповещения_ = Новый Структура("Медпрограммы", Медпрограммы_);
			Оповещение_ = Новый ОписаниеОповещения("ПослеОтменыУслуг", ЭтотОбъект, ПараметрыОповещения_);
			ОткрытьФорму(
				"Обработка.ОтменаУслугЗаказа.Форма",
				ПараметрыФормы_,
				ЭтотОбъект,,,,
				Оповещение_,
				РежимОткрытияОкнаФормы.БлокироватьОкноВладельца
			);
		Иначе
			ПослеОтменыУслуг(Истина, Новый Структура("Медпрограммы", Медпрограммы_));
		КонецЕсли;
	КонецЕсли;
	
	ОбновитьТаблицуДанных();
КонецПроцедуры



&НаКлиенте
&Вместо("ПослеОтменыУслуг")
Процедура ПослеОтменыУслуг_Расширение(Результат, ДополнительныеПараметры) Экспорт
	Если Результат = Истина Тогда
		Отмена_ = СоздатьОтменуМедпрограмм(
			ЭтотОбъект.МедицинскаяКарта, ЭтотОбъект.Пациент, ДополнительныеПараметры.Медпрограммы
		);
		Если ЗначениеЗаполнено(Отмена_) Тогда
			ОбновитьТаблицуДанных();
			Элементы.ЗаказанныеМедпрограммы.Обновить();
			Элементы.Медпрограммы.Обновить();
		КонецЕсли;
	КонецЕсли;
КонецПроцедуры


&НаКлиенте
Процедура СоздатьЗаказПоМедпрограмме(Команда)
	Строка_ = Элементы.ЗаказанныеМедпрограммы.ТекущиеДанные;
	Если Строка_ <> Неопределено И ЗначениеЗаполнено(Строка_.ЗаказМП) Тогда
		ДокМедПрограммы = Строка_.ЗаказМП;
		СоздатьЗаказПоМедпрограммеНаСервере(ДокМедПрограммы,Строка_.УИДМедпрограммы,Строка_.Медпрограмма);
	КонецЕсли;
КонецПроцедуры


&НаСервере
Процедура СоздатьЗаказПоМедпрограммеНаСервере(пДокМедПрограммы,пУИДМедпрограммы,пМедпрограмма)
	Если ЗначениеЗаполнено(пДокМедПрограммы.Заказ) Тогда
		 Сообщить("По медпрограмме уже создан заказ");
	КонецЕсли;	
	ТЧ = пДокМедПрограммы.МедицинскиеПрограммы;
	Отбор_ = ТЧ.НайтиСтроки(Новый структура("УИДМедпрограммы",пУИДМедпрограммы));
	Если Отбор_.Количество()= 0 Тогда
		//Так не должно быть
		Сообщить("Медпрограмма не найдена, обратитесь к разработчикам");
	Иначе	
		Соглашение_ = Отбор_[0].Соглашение;
		Заказ_ = УКД_Румба.СоздатьЗаказПоПакету(пДокМедПрограммы.МедицинскаяКарта,пМедпрограмма,Соглашение_,Соглашение_.Организация,пУИДМедпрограммы);	
		ОбДок = пДокМедПрограммы.ПолучитьОбъект();
		ОбДок.Заказ = Заказ_;
		ОбДок.Записать();
		Попытка
			ОбДок.Записать(РежимЗаписиДокумента.Проведение);
		Исключение
			Сообщить(ОписаниеОшибки());
		КонецПопытки;
	КонецЕсли;	
		
КонецПроцедуры
