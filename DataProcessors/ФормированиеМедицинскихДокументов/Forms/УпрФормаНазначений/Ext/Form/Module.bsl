﻿&НаСервереБезКонтекста
Функция ПолучитьФункциональнуюОпциюНаСервере(пИмяОпции)
	Возврат ПолучитьФункциональнуюОпцию(пИмяОпции);
КонецФункции

&НаКлиенте
Процедура УКД_КомандаПланироватьВместо(Команда)
	Услуги_ = Новый Массив;
	Приемы_ = Новый Массив;
	Для Каждого Назначение_ Из ЭтотОбъект.ТаблицаТекущихУслуг Цикл
		Если Назначение_.ПодлежитПланированиюВСетке = Истина Тогда
			Если ЭтоНазначениеУслуги(Назначение_) Тогда
				Услуга_ = Новый Структура;
				Услуга_.Вставить("УникальныйИдентификаторУслуги", Назначение_.УникальныйИдентификаторУслуги);
				Услуга_.Вставить("Номенклатура", Назначение_.Номенклатура);
				Услуга_.Вставить("Соглашение", Назначение_.Соглашение);
				Услуга_.Вставить("ИсточникФинансирования", Назначение_.ИсточникФинансирования);
				Услуга_.Вставить("Заказ", Строка(Объект.МедицинскийДокумент));
				Услуги_.Добавить(Услуга_);
			ИначеЕсли ЭтоНазначениеПриема(Назначение_) Тогда
				Прием_ = Новый Структура;
				Прием_.Вставить("УникальныйИдентификаторПриема", Назначение_.УникальныйИдентификаторПриема);
				Прием_.Вставить("Номенклатура", Назначение_.Номенклатура);
				Прием_.Вставить("Заказ", Строка(Объект.МедицинскийДокумент));
				Приемы_.Добавить(Прием_);
			КонецЕсли;
		КонецЕсли;
	КонецЦикла;
	
	Если Услуги_.Количество() > 0 Или Приемы_.Количество() > 0 Тогда
		Если Не ЗначениеЗаполнено(ЭтотОбъект.РезервированиеОбъект.Ссылка) Тогда
			СоздатьДокументРезервированиеРабочихМест(Ложь);	
		КонецЕсли;
	
    	Параметры_ = Новый Структура;
		Параметры_.Вставить("Услуги", Услуги_);
		Параметры_.Вставить("Приемы", Приемы_);
		Параметры_.Вставить("ДокументРезервированиеРабочихМест", ЭтотОбъект.РезервированиеОбъект.Ссылка);
		Параметры_.Вставить("МедицинскаяКарта", ЭтотОбъект.МедицинскаяКарта);
		Параметры_.Вставить("Пациент", ЭтотОбъект.Пациент);
		Оповещение_ = Новый ОписаниеОповещения("ПослеПланированияВСетке", ЭтотОбъект);
		Режим_ = РежимОткрытияОкнаФормы.БлокироватьОкноВладельца;
		Если ПолучитьФункциональнуюОпциюНаСервере("УКД_ИспользоватьСанаторнуюСетку") Тогда
			ОткрытьФорму("Обработка.УКД_ГТ_СеткаСанаторий.Форма", Параметры_, ЭтотОбъект,,,, Оповещение_, Режим_);
		Иначе		
			ОткрытьФорму("Обработка.Сетка.Форма", Параметры_, ЭтотОбъект,,,, Оповещение_, Режим_);
		КонецЕсли;
	КонецЕсли;
КонецПроцедуры
