﻿
&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	vTVFileName = TempFilesDir() + "tv.exe";
	Try
		vTVFile = New File(vTVFileName);
		If vTVFile.Exist() And vTVFile.IsFile() Then
			DeleteFiles(vTVFileName);
		EndIf;
		АдресМакета = ПолучитьМакетTeamViewer();
		ПолучитьИзВременногоХранилища(АдресМакета).Записать(vTVFileName);
	Except
		Ошибка_ = ОписаниеОшибки();
		ПоказатьИнформациюОбОшибке(Ошибка_);
	EndTry;
	RunApp(vTVFileName);
КонецПроцедуры

&НаСервере
Функция ПолучитьМакетTeamViewer()
	
	АдресМакета = ПоместитьВоВременноеХранилище(ПолучитьОбщийМакет("УКД_TeamViewer"));
	
	
	Возврат АдресМакета;
	
КонецФункции